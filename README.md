# SomeRequests

## Let's assume we have a blog with some articles

### Define the schema from articles for the following requests

### Requests

* Get all articles ordered by Alpha
* Get 5 last articles that have been created
* Get 5 last articles created and published by Author A
* Update the name from Author A to XX